<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Customer;
use App\QuoteMeta;

class Quote extends Model
{
    protected $table = 'quotes';
    protected $guarded = [];

    //Customer and Quote relation
    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }

    //relation between Quote and Quote meta
    public function quote_meta()
    {
        return $this->hasMany(QuoteMeta::class, 'quote_id');
    }

    
}
