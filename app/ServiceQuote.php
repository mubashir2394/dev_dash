<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Service;

class ServiceQuote extends Model
{
    protected $guarded = [];
    protected $table = 'service_quote';

    //Service and Quote relation
    public function service()
    {
        return $this->belongsTo(Service::class, 'service_id');
    }
}
