<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\ServicePackage;
use App\Coupon;

class Order extends Model
{
    protected $guarded = [];
    protected $table = 'site_orders';
    
    public function createOrder($request){
        $order = new Order;
        $order->customerName = $request->first. " ".$request->last;
        $order->customerEmail = $request->email;
        $order->customerPhone = $request->phone;
        $order->address = $request->address;
        $order->country = $request->country;
        $order->package = implode(',', $request->packages);
        $order->status = 'unpaid';
        $order->applied_coupon =  empty($request->applied_coupon) ? "":$request->applied_coupon;
        $order->comment =  empty($request->comment) ? null:$request->comment;

        $packages = ServicePackage::find($request->packages);
        $total_amount = 0;
        foreach($packages as $key =>  $package){
           $total_amount += $package->price;
        }

        if(!empty($order->applied_coupon)){
            $coupon = Coupon::find($order->applied_coupon);
            $discount = $total_amount * ($coupon->percent/100);
        }
        else{
            $discount = 0;
        }

        $order->amount = round($total_amount - $discount);

        $data = Order::create([
                                'customerName'=> $order->customerName,
                                'customerEmail'=> $order->customerEmail,
                                'customerPhone'=> $order->customerPhone,
                                'address'=> $order->address,
                                'country'=> $order->country,
                                'package'=> $order->package,
                                'status'=> $order->status,
                                'applied_coupon'=> $order->applied_coupon,
                                'comment'=> $order->comment,
                                'amount'=> $order->amount
                            ]);

        return $data->id;
    }

   public function updateOrder($request, $id){
        $order = new Order;
        $order->customerName = $request->customerName;
        $order->customerEmail = $request->email;
        $order->customerPhone = $request->phone;
        $order->address = $request->address;
        $order->country = $request->country;
        $order->package = implode(',', $request->packages);
        // $order->status = 'unpaid';
        $order->applied_coupon =  empty($request->applied_coupon) ? "":$request->applied_coupon;
        $order->comment =  empty($request->comment) ? null:$request->comment;

        $packages = ServicePackage::find($request->packages);
        $amount = 0;
        foreach($packages as $key =>  $package){
           $amount += $package->price;
        }

        $order->amount = $amount;

        Order::where('id',$id)->update([
                                        'customerName'=> $order->customerName,
                                        'customerEmail'=> $order->customerEmail,
                                        'customerPhone'=> $order->customerPhone,
                                        'address'=> $order->address,
                                        'country'=> $order->country,
                                        'package'=> $order->package,
                                        'applied_coupon'=> $order->applied_coupon,
                                        'comment'=> $order->comment,
                                        'amount'=> $order->amount
                                    ]);
    }

}
