<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\ServiceQuote;

class Service extends Model
{
    protected $guarded = [];
    protected $table = 'services';
    
    public function createService($request){
        $service = new Service;
        $service->name = $request->name;
        $service->save();
    }

    public function updateService($request, $id){
        $service = new Service;
        $service->name = $request->name;

        Service::where('id',$id)->update(['name'=> $request->name
                                            ]);
    }

    public function service_quotes(){
        return $this->hasMany(ServiceQuote::class, 'service_id');
    }
    
}
