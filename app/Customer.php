<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Customer extends Model
{   protected $guarded = [];
    protected $table = 'customers';

    public function createCustomer($request){

        $validator = Validator::make($request->all(), [
            'email' => 'required|unique:customers,email',
        ]);

        if (!$validator->fails()) {
            $customer = new Customer;
            $customer->first_name = $request->first;
            $customer->last_name = $request->last? $request->last:'';
            $customer->country = $request->country? $request->country:'';
            $customer->state = isset($request->state) ? $request->state:'';
            $customer->address = $request->address? $request->address:'';
            $customer->phone = $request->phone? $request->phone:'';
            $customer->email = $request->email;
            $customer->save();
            return $customer->id;
            }
        }

        

    public function updateCustomer($request, $id){
        $customer = new Customer;
        $customer->first_name = $request->first;
        $customer->last_name = $request->last;
        $customer->country = $request->country;
        $customer->state = $request->state;
        $customer->address = $request->address;
        $customer->phone = $request->phone;
        $customer->email = $request->email;

        Customer::where('id',$id)->update(['first_name'=> $request->first, 
                                            'last_name'=> $request->last, 
                                            'country'=> $request->country, 
                                            'state'=> $request->state, 
                                            'address'=> $request->address,
                                            'phone'=> $request->phone, 
                                            'email'=> $request->email,
                                            ]);
    }

    // public function createCustomerFromQuotes(){
    //     $id = DB::table('customers')-> insertGetId(array(
    //         'email_id' => $email_id,
    //         'name' => $name,
    // ));
    // }
}
