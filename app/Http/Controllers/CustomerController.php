<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Customer;
use Carbon\Carbon;
class CustomerController extends Controller

{
    public $module;
    public $model;
    public $assignedData = [];
    
    public function __construct()
    {  
        $this->middleware('auth');
        
        $this->module = 'customer';
        $this->model = new Customer;
        $this->assignedData['module'] = $this->module;
    }

    //index function
    public function index(){
        $this->assignedData['customers'] = $this->model::where('deleted_at',null)->get();
        return view($this->module.'.'.__FUNCTION__, $this->assignedData);
    }

    //view function
    public function view($id){
        $this->assignedData['customer'] = $this->model::find($id);
        return view($this->module.'.'.__FUNCTION__,$this->assignedData);
    }

    //add function
    public function add(){
        return view($this->module.'.'.__FUNCTION__, $this->assignedData);
    }

    //Store function
    public function store(Request $request){

        $validator = Validator::make($request->all(), [
            'first' => 'required',
            'last' => 'required',
            'country' => 'required',
            'state' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'email' => 'required|unique:customers,email',
        ]);

        if ($validator->fails()) {
            return redirect($this->module.'s/add')
                        ->withErrors($validator)
                        ->withInput();
        }

        $this->model->createCustomer($request);

        return redirect()->route($this->module)->with('status', $this->module.' Added');
    }

    //Update function
    public function update($id){

        $this->assignedData['customer'] = $this->model::find($id);
        return view($this->module.'.'.__FUNCTION__,$this->assignedData);
    }

    public function _update(Request $request, $id){
        
        $validator = Validator::make($request->all(), [
            'first' => 'required',
            'last' => 'required',
            'country' => 'required',
            'state' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'email' => 'required|unique:customers,email,'.$id
        ]);

        if ($validator->fails()) {
            return redirect($this->module.'s/edit/'.$id)
                        ->withErrors($validator)
                        ->withInput();
        }

        $this->model->updateCustomer($request, $id);
        
        return redirect()->route($this->module)->with('status', $this->module.' Updated');
        
        
    }

    //delete function
    public function delete($id){
        $currentTime = date("Y-m-d h:i:s");
        $this->model::where('id', $id)->update(['deleted_at'=> $currentTime]);

        return redirect()->route($this->module)->with('status', $this->module.' Deleted');

    }
}
