<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use App\Coupon;

use Illuminate\Http\Request;

class CouponController extends Controller
{
    public $module;
    public $model;
    public $assignedData = [];
    
    public function __construct()
    {  
        $this->module = 'coupon';
        $this->model = new Coupon;
        $this->assignedData['module'] = $this->module;
    }

    //index function
    public function index(){
        $this->assignedData['coupons'] = $this->model::where('deleted_at',null)->get();
        return view($this->module.'.'.__FUNCTION__, $this->assignedData);
    }

    //add function
    public function add(){
        return view($this->module.'.'.__FUNCTION__, $this->assignedData);
    }

    //store function
    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'code' => 'required|unique:servicecoupons,code',
            'percent' => 'required|max:3',
            'limit' => 'required',
        ],$messages = [
            'max' => 'The :attribute field can not be greater than 100.',
        ]);

        if ($validator->fails()) {
            return redirect($this->module.'s/add')
                        ->withErrors($validator)
                        ->withInput();
        }

        $this->model->createCoupon($request);

        return redirect()->route($this->module)->with('status', $this->module.' Added');
    }

    //update function
    public function update($id){
        $this->assignedData['coupon'] = $this->model::find($id);
        return view($this->module.'.'.__FUNCTION__, $this->assignedData);
    }

    //_update function
    public function _update(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'code' => 'required|unique:servicecoupons,code,'.$id,
            'percent' => 'required|max:3',
            'limit' => 'required',
        ],$messages = [
            'max' => 'The :attribute field can not be greater than 100.',
        ]);

        if ($validator->fails()) {
            return redirect($this->module.'s/edit/'.$id)
                        ->withErrors($validator)
                        ->withInput();
        }

        $this->model->updateCoupon($request, $id);
        
        return redirect()->route($this->module)->with('status', $this->module.' Updated');
    }

    //delete function
    public function delete($id){
        $currentTime = date("Y-m-d h:i:s");
        $this->model::where('id', $id)->update(['deleted_at'=> $currentTime]);

        return redirect()->route($this->module)->with('status', $this->module.' Deleted');
    }
}
