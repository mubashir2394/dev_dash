<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Order;
use App\ServicePackage;
use App\Coupon;
use App\Customer;
use Carbon\Carbon;
use Session;
use Stripe;
use PDF;

class OrderController extends Controller
{
    public $module;
    public $model;
    public $assignedData = [];
    
    public function __construct()
    {  
        $this->module = 'order';
        $this->model = new Order;
        $this->assignedData['module'] = $this->module;
    }

    //index function
    public function index(){
        $this->assignedData['orders'] = $this->model::where('deleted_at', null)->get();
        return view($this->module.'.'.__FUNCTION__, $this->assignedData);
    }

    //add function
    public function add(){
        $this->assignedData['packages'] = ServicePackage::where('deleted_at', null)->get();
        $this->assignedData['coupons'] = Coupon::where('deleted_at', null)->where('status', 1)->get();
        return view($this->module.'.'.__FUNCTION__, $this->assignedData);
    }

    //store function
    public function store(Request $request){
        // dd($request);
        $validator = Validator::make($request->all(), [
            'first' => 'required',
            'last' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'country' => 'required',
            'packages' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect($this->module.'s/add')
                        ->withErrors($validator)
                        ->withInput();
        }

        $customer = new Customer;
        $customer->createCustomer($request);

        $order_id = $this->model->createOrder($request);

        return redirect('/'.$this->module.'s/cart/'.$order_id);
    }

    // cart function
    public function cart(Request $request, $id){
        $order = Order::find($id);
        $order->packages = explode(",", $order->package);
        $packages = ServicePackage::find($order->packages);
        

        $this->assignedData['order'] = $order;
        $sub_total = 0;
        foreach($packages as $key =>  $package){
           $package_data[$key]['package'] = $package->name;
           $package_data[$key]['service'] = $package->service->name;
           $package_data[$key]['price'] = $package->price;

           $sub_total += $package->price;
        }
        
        if(!empty($order->applied_coupon)){
            $coupon = Coupon::find($order->applied_coupon);
            $discount = $sub_total * ($coupon->percent/100);
        }
        else{
            $coupon = null;
            $discount = 0;
        }

        $total_amount = $order->amount;

        $this->assignedData['order']['order_id'] = $id;
        $this->assignedData['order']['package_data'] = $package_data;
        $this->assignedData['order']['sub_total'] = $sub_total;
        $this->assignedData['order']['coupon'] = $coupon;
        $this->assignedData['order']['discount'] = $discount;
        $this->assignedData['order']['total_amount'] = $total_amount;

        $date = new Carbon();
        $date::parse();
        $this->assignedData['order']['date'] = $date->format('jS F Y'); 

        //PDF Generation 
        if($request->pdf == 1){
            $pdf = PDF::loadView($this->module.'.pdf', $this->assignedData);
            return $pdf->download('pdfview.pdf');
        }

        return view($this->module.'.'.__FUNCTION__, $this->assignedData);
    }

    //update function
    public function update($id){
        $order = $this->model::find($id);
        $order->packages = explode(",", $order->package);
        // dd($order);
        $this->assignedData['order'] = $order;
        $this->assignedData['packages'] = ServicePackage::where('deleted_at', null)->get();
        $this->assignedData['coupons'] = Coupon::where('deleted_at', null)->where('status', 1)->get();
        return view($this->module.'.'.__FUNCTION__, $this->assignedData);
    }

    //_update function
    public function _update(Request $request, $id){
        // dd($request);
        $validator = Validator::make($request->all(), [
            'customerName' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'country' => 'required',
            'packages' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect($this->module.'s/edit/'.$id)
                        ->withErrors($validator)
                        ->withInput();
        }

        $this->model->updateOrder($request, $id);
        
        return redirect()->route($this->module)->with('status', $this->module.' Updated');
    }

    //delete function
    public function delete($id){
        $currentTime = date("Y-m-d h:i:s");
        $this->model::where('id', $id)->update(['deleted_at'=> $currentTime]);

        return redirect()->route($this->module)->with('status', $this->module.' Deleted');
    }

    //Billing page function
    public function stripe($id)
    {
        return view($this->module.'.'.__FUNCTION__, $this->assignedData);
    }

    //payment functionality
    public function stripePost(Request $request)
    {
        Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        Stripe\Charge::create ([
                "amount" => 100*100,
                "currency" => "INR",
                "source" => $request->stripeToken,
                "description" => "This payment is testing purpose of websolutionstuff.com",
        ]);
   
        Session::flash('success', 'Payment Successful !');
           
        return back();
    }

}
