<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Quote;
use App\Service;
use App\ServiceQuote;
use App\Customer;
use App\QuoteMeta;

class QuoteController extends Controller
{
    public $module;
    public $model;
    public $assignedData = [];
    
    public function __construct()
    {  
        $this->module = 'quote';
        $this->model = new Quote;
        $this->assignedData['module'] = $this->module;
    }

    //index function
    public function index(){
        $quotes = $this->model::groupBy('customer_id')->where('deleted_at', null)->get();

        foreach($quotes as $key => $quote){
            $arr = [];
            foreach($quote->quote_meta as $meta){
                $arr[] = $meta->service;
            }
            $arr = array_unique($arr);
            $arr = implode(",",$arr);
            $quote->meta = $arr;
        }
        $this->assignedData['quotes'] = $quotes;
        return view($this->module.'.'.__FUNCTION__,  $this->assignedData);
    }

    //view function
    public function view($id){
        $quotes = $this->model::find($id);
        $quotes->meta = $quotes->quote_meta;

        $this->assignedData['quotes'] = $quotes;
        return view($this->module.'.'.__FUNCTION__,  $this->assignedData);
    }

    //add function
    public function add(){
        $this->assignedData['services']['all_services'] = Service::where('deleted_at', null)->get();
        $this->assignedData['services']['module'] = $this->module;
        return view($this->module.'.'.__FUNCTION__, $this->assignedData);
    }

    public function submit(Request $request){
   
        $customer = new Customer;
        $customer_id = $customer->createCustomer($request);

        if($customer_id == null){
            $customer = $customer::select('id')->where('email', $request->email)->first();
            $customer_id = $customer->id;
        }

        // creating quote
        $quote = $this->model::create([
            'name' => $request->first,
            'email' => $request->email,
            'asin' => $request->asin,
            'budget' => $request->budget,
            'customer_id' => $customer_id,
        ]);

        // creating quote_meta
        foreach($request->photography as $photoObj){
            $quote_meta = QuoteMeta::create([
                'quote_id' => $quote->id,
                'service' => $photoObj['S'],
                'quote_key' => $photoObj['Q'],
                'quote_value' => $photoObj['A'],
            ]);
        }

        foreach($request->videography as $videoObj){
            $quote_meta = QuoteMeta::create([
                'quote_id' => $quote->id,
                'service' => $videoObj['S'],
                'quote_key' => $videoObj['Q'],
                'quote_value' => $videoObj['A'],
            ]);
        }


    }

    //delete function
    public function delete($id){
        $currentTime = date("Y-m-d h:i:s");
        $this->model::where('id', $id)->update(['deleted_at'=> $currentTime]);

        QuoteMeta::where('id', $id)->update(['deleted_at'=> $currentTime]);

        return redirect()->route($this->module)->with('status', $this->module.' Deleted');

    }


}
