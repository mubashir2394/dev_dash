<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ServicePackage;
use App\Service;
use Illuminate\Support\Facades\Validator;

class ServicePackageController extends Controller
{
    public $module;
    public $model;
    public $assignedData = [];
    
    public function __construct()
    {  
        $this->module = 'package';
        $this->model = new ServicePackage;
        $this->assignedData['module'] = $this->module;
    }

    //index function
    public function index(){
        $packages = $this->model::where('deleted_at',null)->get();
        foreach($packages as $package){
            $package->service_name = $package->service->name;
        }
        $this->assignedData['packages'] = $packages;
        return view($this->module.'.'.__FUNCTION__, $this->assignedData);
    }

    //view function
    public function view($id){
        $package = $this->model::find($id);
        $package->service_name = $package->service->name;
        $this->assignedData['package'] = $package;

        return view($this->module.'.'.__FUNCTION__, $this->assignedData);
    }

    //add function
    public function add(){
        $this->assignedData['services'] = Service::where('deleted_at',null)->get();
        return view($this->module.'.'.__FUNCTION__, $this->assignedData);
    }

    //store function
    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:service_packages,name',
            'service_id' => 'required',
            'price' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect($this->module.'s/add')
                        ->withErrors($validator)
                        ->withInput();
        }
        
        $this->model->createPackage($request);

        return redirect()->route($this->module)->with('status', $this->module.' Added');
    }

    //update function
    public function update($id){
        $this->assignedData['services'] = Service::where('deleted_at',null)->get();

        $package = $this->model::find($id);
        $package->service_name = $package->service->name;
        $this->assignedData['package'] = $package;

        return view($this->module.'.'.__FUNCTION__, $this->assignedData);
    }

    //_update function
    public function _update(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:service_packages,name,'.$id,
            'service_id' => 'required',
            'price' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect($this->module.'s/edit/'.$id)
                        ->withErrors($validator)
                        ->withInput();
        }

        $this->model->updatePackage($request, $id);

        return redirect()->route($this->module)->with('status', $this->module.' Updated');
    }

    //delete function
    public function delete($id){
        $currentTime = date("Y-m-d h:i:s");
        $this->model::where('id', $id)->update(['deleted_at'=> $currentTime]);

        return redirect()->route($this->module)->with('status', $this->module.' Deleted');
    }
}
