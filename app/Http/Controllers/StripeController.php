<?php

namespace App\Http\Controllers;
use Session;
use Stripe;
use App\Order;

use Illuminate\Http\Request;

class StripeController extends Controller
{
    public $module;
    public $model;
    public $assignedData = [];
    
    public function __construct()
    {  
        $this->module = 'stripe';
        $this->assignedData['module'] = $this->module;
    }

    public function stripe($id)
    {
        $order = Order::find($id);
        if($order->status == 'paid'){
            return redirect('orders/cart/'.$id);
        }
        $this->assignedData['order']['amount'] = $order->amount;
        $this->assignedData['order']['order_id'] = $id;
        $this->assignedData['order']['customerName'] = $order->customerName;

        return view($this->module.'.'.__FUNCTION__, $this->assignedData);
    }

    public function stripePost(Request $request, $id)
    {
        Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        Stripe\Charge::create ([
                "amount" => $request->amount * 100,
                "currency" => "USD",
                "source" => $request->stripeToken,
                "description" => "This payment is testing purpose",
        ]);

        Order::where('id', $id)->update(['status' => 'paid']);

        return $this->success($request->customerName);

        Session::flash('success', 'Payment Successful !');
           
        return back();
    }

    public function success($customerName){   
        $this->assignedData['customerName'] = $customerName;
        return view($this->module.'.'.__FUNCTION__, $this->assignedData);
    }
}