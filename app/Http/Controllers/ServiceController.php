<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Service;

class ServiceController extends Controller
{
    public $module;
    public $model;
    public $assignedData = [];
    
    public function __construct()
    {  
        $this->module = 'service';
        $this->model = new Service;
        $this->assignedData['module'] = $this->module;
    }

    //index function
    public function index(){
        $this->assignedData['services'] = $this->model::where('deleted_at',null)->get();
        return view($this->module.'.'.__FUNCTION__, $this->assignedData);
    }

    //add function
    public function add(){
        return view($this->module.'.'.__FUNCTION__, $this->assignedData);
    }

    //store function
    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:services,name'
        ]);

        if ($validator->fails()) {
            return redirect($this->module.'s/add')
                        ->withErrors($validator)
                        ->withInput();
        }

        $this->model->createService($request);

        return redirect()->route($this->module)->with('status', $this->module.' Added');
    }

    //update function
    public function update($id){
        $this->assignedData['service'] = $this->model::find($id);
        return view($this->module.'.'.__FUNCTION__, $this->assignedData);
    }

    //_update function
    public function _update(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:services,name,'.$id,
        ]);

        if ($validator->fails()) {
            return redirect($this->module.'s/edit/'.$id)
                        ->withErrors($validator)
                        ->withInput();
        }

        $this->model->updateService($request, $id);

        return redirect()->route($this->module)->with('status', $this->module.' Updated');
    }

    //delete function
    public function delete($id){
        $currentTime = date("Y-m-d h:i:s");
        $this->model::where('id', $id)->update(['deleted_at'=> $currentTime]);

        return redirect()->route($this->module)->with('status', $this->module.' Deleted');
    }
}
