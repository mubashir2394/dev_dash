<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use App\Order;

class DashboardController extends Controller
{
    public $module;
    // public $model;
    public $assignedData = [];
    
    public function __construct()
    {  
        $this->middleware('auth');
        
        $this->module = 'dashboard';
        // $this->model = new Dashboard;
        $this->assignedData['module'] = $this->module;
    }

    //index function
    public function dashboard(){
        $this->assignedData['customers'] = Customer::where('deleted_at',null)->where('created_at')->get();
        return view(__FUNCTION__, $this->assignedData);
    }
}
