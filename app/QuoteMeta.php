<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuoteMeta extends Model
{
    protected $table = 'quote_meta';
    protected $guarded = [];
}
