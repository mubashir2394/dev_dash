<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Service;

class ServicePackage extends Model
{
    protected $guarded = [];
    protected $table = 'service_packages';
    
    public function createPackage($request){
        $package = new ServicePackage;
        $package->name = $request->name;
        $package->service_id = $request->service_id;
        $package->price = $request->price;
        $package->save();
    }

    public function updatePackage($request, $id){
        $package = new ServicePackage;
        $package->name = $request->name;
        $package->service_id = $request->service_id;
        $package->price = $request->price;

        ServicePackage::where('id',$id)->update([
                                        'name'=> $request->name,
                                        'service_id'=> $request->service_id,
                                        'price'=> $request->price,
                                        ]);
    }

    //Package and Service relation
    public function service()
    {
        return $this->belongsTo(Service::class, 'service_id');
    }
}
