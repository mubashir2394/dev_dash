<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{   
    protected $guarded = [];
    protected $table = 'servicecoupons';

    public function createCoupon($request){
        $coupon = new Coupon;
        $coupon->code = $request->code;
        $coupon->percent = $request->percent;
        $coupon->status = $request->status;
        $coupon->limit = $request->limit;
        $coupon->save();
    }

    public function updateCoupon($request, $id){
        $coupon = new Coupon;
        $coupon->code = $request->code;
        $coupon->percent = $request->percent;
        $coupon->status = $request->status;
        $coupon->limit = $request->limit;

        Coupon::where('id',$id)->update(['code'=> $request->code, 
                                            'percent'=> $request->percent, 
                                            'status'=> $request->status, 
                                            'limit'=> $request->limit
                                            ]);
    }
}
