@extends('layout.master')

@push('plugin-styles')
  <link href="{{ asset('assets/plugins/datatables-net/dataTables.bootstrap4.css') }}" rel="stylesheet" />
  <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" />
@endpush

@section('content')

<div class="row">
  <div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h6 class="card-title">Create {{$module}}</h6>
        <form method="post" action="{{ url('/'.$module.'s/store') }}">
        {{ csrf_field() }}
          <div class="form-group">
            <label>Service Name</label>
            <input type="text" class="form-control" name="name" autocomplete="off" placeholder="Photography"/>
            @error('name')
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror
          </div>
          <!-- <input type="hidden" class="form-control" name="do_post" value="1"/> -->

          <button type="submit" class="btn btn-primary mr-2">Create</button>
          <a class="btn btn-light" href="{{ url('/'.$module.'s') }}">Cancel</a>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection

@push('plugin-scripts')
  <!-- <script src="{{ asset('assets/plugins/datatables-net/jquery.dataTables.js') }}"></script> -->
  <script src="{{ asset('assets/plugins/datatables-net-bs4/dataTables.bootstrap4.js') }}"></script>
@endpush

@push('custom-scripts')
  <script src="{{ asset('assets/js/data-table.js') }}"></script>
@endpush