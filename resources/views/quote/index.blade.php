@extends('layout.master')

@push('plugin-styles')
  <link href="{{ asset('assets/plugins/datatables-net/dataTables.bootstrap4.css') }}" rel="stylesheet" />
  <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" />
  <!-- <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css"> -->
@endpush

@section('content')
@if (session('status'))
    <div class="alert alert-success">
        {{ ucfirst(session('status')) }}
    </div>
@endif
<div class="row">
  <div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="container-fluid ml-0 pl-0">
            <div class="row">
                <div class="col-md-10">
                    <h6 class="card-title">{{$module}}s List</h6>
                </div>
                <div class="col-md-2">
                    <a href="{{ url('/'.$module.'s/add') }}" class="add-button">
                        <span class="link-title" data-toggle="tooltip" title="Add New Coupon">Add New Quote</span>
                    </a>
                </div>
            </div>
        </div>
        <!-- <h6 class="card-title">Customers Table</h6> -->
        <!-- <a href="{{ url('/customers/view') }}" class="">
                        <span class="link-title" data-toggle="tooltip" title="View">
                        <i class="link-icon" data-feather="eye"></i></span>
        </a> -->
        
        <div class="table-responsive">
          <table id="dataTableExample" class="table">
            <thead>
              <tr>
                <th>Customer Name</th>
                <th>Email</th>
                <th>ASIN</th>
                <th>Budget</th>
                <th>Services</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              
            @foreach($quotes as $quote)
                <tr>
                    <td>{{$quote->name}}</td>
                    <td>{{$quote->email}}</td>
                    <td>{{$quote->asin}}</td>
                    <td>{{$quote->budget}}</td>
                    <td>{{$quote->meta}}</td>
                    <td>
                        <a href="{{ url('/'.$module.'s/view/'.$quote->id) }}" class="">
                            <span class="link-title" data-toggle="tooltip" title="View">View |</span>
                        </a>
                        <a href="{{ url('/'.$module.'s/edit/'.$quote->id) }}" class="">
                            <span class="link-title" data-toggle="tooltip" title="Edit">Edit |</span>
                        </a>
                        <!-- <a href="{{ url('/'.$module.'s/delete/'.$quote->id) }}" class="">
                            <span class="link-title" data-toggle="tooltip" title="Remove"></i>Remove</span>
                        </a> -->
                        <a style="color:#0056b3;" id="btnModalDel"  data-id="{{$quote->id}}">
                          <span class="link-title" data-toggle="tooltip" title="Remove"></i>Remove</span>
                        </a>
                    </td>
                    </tr>
                    @endforeach
              
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="confirmationModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Delete Confirmation</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          Are you sure you want to delete this {{$module}} ?
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">No, Cancel</button>
          <a id="deleteCustomer" href="{{ url('/'.$module.'s/delete/'.$quote->id) }}" class="btn btn-primary">
            Yes Delete !
          </a>
        </div>
      </div>
    </div>
</div>
@endsection

@push('plugin-scripts')
  <script src="{{ asset('assets/plugins/datatables-net/jquery.dataTables.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-net-bs4/dataTables.bootstrap4.js') }}"></script>
@endpush

@push('custom-scripts')
  <script src="{{ asset('assets/js/data-table.js') }}"></script>
  <script>

$(document).ready(function(){
    $(document).on('click','#btnModalDel',function(){

      var cusId = $(this).data('id');
      var DelURL = 'quotes/delete/'+cusId;
    
      $('#deleteCustomer').attr('href',DelURL);

      $('#confirmationModal').modal('show');
    });
});


</script>
@endpush