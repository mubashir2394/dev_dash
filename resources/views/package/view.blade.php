@extends('layout.master')

@push('plugin-styles')
  <link href="{{ asset('assets/plugins/datatables-net/dataTables.bootstrap4.css') }}" rel="stylesheet" />
  <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" />
  <!-- <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css"> -->
@endpush

@section('content')

<div class="row">
  <div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="container-fluid ml-0 pl-0">
            <div class="row">
                <div class="col-md-12">
                    <h6 class="card-title">View {{$module}}</h6>
                </div>
            </div>
            <div class="col-md-4 stretch-card grid-margin grid-margin-md-0">
              <div class="card package-center">
                <div class="card-body">
                  <h5 class="text-center text-uppercase mt-3 mb-4">{{$package->name}}</h5>
                  <i data-feather="award" class="text-primary icon-xxl d-block mx-auto my-3"></i>
                  <h3 class="text-center font-weight-light">${{$package->price}}</h3>
                  <!-- <p class="text-muted text-center mb-4 font-weight-light">per month</p> -->
                  <!-- <h6 class="text-muted text-center mb-4 font-weight-normal">Up to 25 units</h6> -->
                  <div class="d-flex align-items-center mb-2">
                    <i data-feather="check" class="icon-md text-primary mr-2"></i>
                    <p>Accounting dashboard</p>
                  </div>
                  <div class="d-flex align-items-center mb-2">
                    <i data-feather="check" class="icon-md text-primary mr-2"></i>
                    <p>Invoicing</p>
                  </div>
                  <div class="d-flex align-items-center mb-2">
                    <i data-feather="check" class="icon-md text-primary mr-2"></i>
                    <p>Online payments</p>
                  </div>
                  <div class="d-flex align-items-center mb-2">
                    <i data-feather="x" class="icon-md text-light mr-2"></i>
                    <p class="text-muted">Branded website</p>
                  </div>
                  <div class="d-flex align-items-center mb-2">
                    <i data-feather="x" class="icon-md text-light mr-2"></i>
                    <p class="text-muted">Dedicated account manager</p>
                  </div>
                  <div class="d-flex align-items-center mb-2">
                    <i data-feather="x" class="icon-md text-light mr-2"></i>
                    <p class="text-muted">Premium apps</p>
                  </div>
                  <button class="btn btn-primary d-block mx-auto mt-4">Order Now</button>
                </div>
              </div>
            </div>

        </div>
        
        
      </div>
    </div>
  </div>
</div>
@endsection

@push('plugin-scripts')
  <script src="{{ asset('assets/plugins/datatables-net/jquery.dataTables.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-net-bs4/dataTables.bootstrap4.js') }}"></script>
@endpush

@push('custom-scripts')
  <script src="{{ asset('assets/js/data-table.js') }}"></script>
@endpush