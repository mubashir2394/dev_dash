@extends('layout.master')

@push('plugin-styles')
  <link href="{{ asset('assets/plugins/datatables-net/dataTables.bootstrap4.css') }}" rel="stylesheet" />
  <link href="{{ asset('assets/plugins/select2/select2.min.css') }}" rel="stylesheet" />
  <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" />
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
@endpush

@section('content')
@if (Session::has('success'))
    <div class="alert alert-success">
    {{ Session::get('success') }}
    </div>
@endif
@auth
<div class="row">
@endauth
@guest
<div class="row" style="margin-left: -15.75rem;">
@endguest
   <div class="col-md-12 grid-margin stretch-card">
      <div class="card">
         <div class="card-body">
            <h6 class="card-title">Payment Details</h6>
            <br>
            <form role="form" action="{{ url('/orders/stripe/'.$order['order_id']) }}" method="post" class="require-validation" data-cc-on-file="false" data-stripe-publishable-key="{{ env('STRIPE_KEY') }}" id="payment-form">
               @csrf
                  <div class='form-group required'>
                     <label class='control-label'>Name on Card</label> 
                     <input class='form-control' size='4' type='text'>
                  </div>
                  <div class='form-group required'>
                     <label class='control-label'>Card Number</label> 
                     <input autocomplete='off' class='form-control card-number' size='20' type='text'>
                  </div>                           
                  <div class='form-group cvc required'>
                     <label class='control-label'>CVC</label> 
                     <input autocomplete='off' class='form-control card-cvc' placeholder='ex. 311' size='4' type='text'>
                  </div>
                  <div class='form-group expiration required'>
                     <label class='control-label'>Expiration Month</label> 
                     <input class='form-control card-expiry-month' placeholder='MM' size='2' type='text'>
                  </div>
                  <div class='form-group expiration required'>
                     <label class='control-label'>Expiration Year</label> 
                     <input class='form-control card-expiry-year' placeholder='YYYY' size='4' type='text'>
                  </div>
                  
            <!-- {{-- <div class='form-row row'>
               <div class='col-md-12 error form-group hide'>
                  <div class='alert-danger alert'>Please correct the errors and try
                     again.
                  </div>
               </div>
            </div> --}} -->
               <input class='form-control' name="amount" placeholder='YYYY' size='4' type='hidden' value="{{$order['amount']}}">
               <input class='form-control' name="customerName" placeholder='YYYY' size='4' type='hidden' value="{{$order['customerName']}}">
               <div class="form-row row">
                  <div class="col-xs-12">
                     <button class="btn btn-primary mr-2" type="submit">Pay $ {{$order['amount']}}</button>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
@endsection

@push('plugin-scripts')
  <script src="{{ asset('assets/plugins/datatables-net-bs4/dataTables.bootstrap4.js') }}"></script>
@endpush

@push('custom-scripts')
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>      
   <script type="text/javascript" src="https://js.stripe.com/v2/"></script>

   <script type="text/javascript">
$(function() {
  var $form = $(".require-validation");
  $('form.require-validation').bind('submit', function(e) {
    var $form = $(".require-validation"),
    inputSelector = ['input[type=email]', 'input[type=password]', 'input[type=text]', 'input[type=file]', 'textarea'].join(', '),
    $inputs = $form.find('.required').find(inputSelector),
    $errorMessage = $form.find('div.error'),
    valid = true;
    $errorMessage.addClass('hide');
    $('.has-error').removeClass('has-error');
    $inputs.each(function(i, el) {
        var $input = $(el);
        if ($input.val() === '') {
            $input.parent().addClass('has-error');
            $errorMessage.removeClass('hide');
            e.preventDefault();
        }
    });
    if (!$form.data('cc-on-file')) {
      e.preventDefault();
      Stripe.setPublishableKey($form.data('stripe-publishable-key'));
      Stripe.createToken({
          number: $('.card-number').val(),
          cvc: $('.card-cvc').val(),
          exp_month: $('.card-expiry-month').val(),
          exp_year: $('.card-expiry-year').val()
      }, stripeResponseHandler);
    }
  });

  function stripeResponseHandler(status, response) {
      if (response.error) {
          $('.error')
              .removeClass('hide')
              .find('.alert')
              .text(response.error.message);
      } else {
          /* token contains id, last4, and card type */
          var token = response['id'];
          $form.find('input[type=text]').empty();
          $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
          $form.get(0).submit();
      }
  }
});
</script>
@endpush

