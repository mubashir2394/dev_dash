<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title>AMZ One Step | Invoice</title>
  </head>
  <body>
  <div class="container">
    <div class="row">
    <div class="col-md-12">
        <div class="card">
        <div class="card-body">
            <div class="container-fluid d-flex justify-content-between">
                <div class="col-lg-3 pl-0">
                    <a href="#" class="noble-ui-logo d-block mt-3"><span> BILL TO  </span></a> 
                    <h6 class="text-left">{{$order->customerName}}</h6>
                    <h6 class="text-left">{{$order->address}}</h6>
                    <h6 class="text-left">{{$order->customerEmail}}</h6>
                    <h6 class="text-left">{{$order->customerPhone}}</h6>
                </div>
               
                <div class="col-lg-3 pt-0">
                    <a href="#" class="noble-ui-logo d-block text-right mt-4 mb-2"><span> INVOICE  </span></a> 
                    <h6 class="text-right mb-5 pb-4"># INV-{{$order->order_id}}</h6>
                    <p class="text-right mb-1">Balance Due</p>
                    <h4 class="text-right font-weight-normal">$ {{$order->sub_total}}.00</h4>
                    <!-- <h4 class="text-right font-weight-normal">$ 72,420.00</h4> -->
                    <h6 class="mb-0 mt-3 text-right font-weight-normal mb-2"><span class="text-muted">Invoice Date :</span> {{$order->date}}</h6>
                    <h6 class="text-right font-weight-normal"><span class="text-muted">Due Date :</span> {{$order->date}}</h6>
                </div>
            </div>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <div class="container-fluid mt-5 d-flex justify-content-center w-100">
            <div class="table-responsive w-100">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Package</th>
                        <th class="text-left">Unit Price</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($order->package_data as $key => $row)
                    <tr class="text-right">
                        <td class="text-left">{{$key+1}}</td>
                        <td class="text-left">{{$row['package']}}</td>
                        <td class="text-left">$ {{$row['price']}}.00</td>
                    </tr>
                    @endforeach  
                    </tbody>
                </table>
                </div>
            </div>
            <br>
            <br>
            <br>
            <div class="container-fluid mt-5 w-100">
            <div class="row">
                <div class="col-md-6 ml-auto">
                    <div class="table-responsive">
                    <table class="table">
                        <tbody>
                            <tr>
                            <td>Sub Total</td>
                            <td class="text-right">$ {{$order->sub_total}}.00</td>
                            </tr>
                            <tr>
                            @if($order->coupon == !null)
                            <td>Applied Couopon : <b>{{$order->coupon->code." (".$order->coupon->percent."%)"}}</b></td>
                            <td class="text-right"><b>(-) $ {{$order->discount}}</b></td>
                            @else
                            <td>Applied Couopon : None</td>
                            <td class="text-right">(-) $ 0</td>
                            @endif
                            </tr>
                            <tr class="bg-light">
                            <td class="text-bold-800">Total</td>
                            <td class="text-bold-800 text-right">$ {{$order->total_amount}}</td>
                            </tr>
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
            </div>
        </div>
        </div>
    </div>
    </div>
</div>
    

  </body>
</html>