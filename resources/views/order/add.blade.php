@extends('layout.master')

@push('plugin-styles')
  <link href="{{ asset('assets/plugins/datatables-net/dataTables.bootstrap4.css') }}" rel="stylesheet" />
  <link href="{{ asset('assets/plugins/select2/select2.min.css') }}" rel="stylesheet" />
  <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" />
@endpush

@section('content')
<div class="row">
  <div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h6 class="card-title">Create {{$module}}</h6>
        <form method="post" action="{{ url('/'.$module.'s/store') }}">
        {{ csrf_field() }}
          <div class="form-group">
            <label>First Name</label>
            <input type="text" class="form-control" name="first" autocomplete="off" placeholder="John"/>
            @error('first')
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror
          </div>

          <div class="form-group">
            <label>Last Name</label>
            <input type="text" class="form-control" name="last" autocomplete="off" placeholder="Doe"/>
            @error('last')
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror
          </div>

          <div class="form-group">
            <label>Customer Email</label>
            <input type="text" class="form-control" name="email" autocomplete="off" placeholder="John@gmail.com"/>
            @error('email')
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror
          </div>

          <div class="form-group">
            <label>Customer Phone</label>
            <input type="number" class="form-control" name="phone" autocomplete="off" placeholder="John@gmail.com"/>
            @error('phone')
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror
          </div>

          <div class="form-group">
            <label>Customer Address</label>
            <input type="text" class="form-control" name="address" autocomplete="off" placeholder="John@gmail.com"/>
            @error('address')
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror
          </div>

          <div class="form-group">
            <label>Customer Country</label>
            <input type="text" class="form-control" name="country" autocomplete="off" placeholder="John@gmail.com"/>
            @error('country')
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror
          </div>

          <div class="form-group">
            <label>Select Packages</label>
            <select class="js-example-basic-multiple w-100" multiple="multiple" name="packages[]">
            @foreach($packages as $package){
                    <option class="" value="{{$package->id}}">{{$package->name}}</option>
                 }
              @endforeach
            </select>
            @error('packages')
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror
          </div>

          <div class="form-group">
            <label>Coupon</label>
            <select class="form-control form-control-sm mb-3" name="applied_coupon">
                <option class="" selected value="">Select a coupon</option>
                @foreach($coupons as $coupon){
                    <option class="" value="{{$coupon->id}}">{{$coupon->code}}</option>
                }
                @endforeach
            </select>
          </div>

          <div class="form-group">
            <label>Customer Comment</label>
            <textArea class="form-control" name="comment" autocomplete="off"></textArea>
            @error('customerEmail')
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror
          </div>
     
          
          <!-- <input type="hidden" class="form-control" name="do_post" value="1"/> -->

          <button type="submit" class="btn btn-primary mr-2">Create</button>
          <a class="btn btn-light" href="{{ url('/'.$module.'s') }}">Cancel</a>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection

@push('plugin-scripts')
  <!-- <script src="{{ asset('assets/plugins/datatables-net/jquery.dataTables.js') }}"></script> -->
  <script src="{{ asset('assets/plugins/datatables-net-bs4/dataTables.bootstrap4.js') }}"></script>
@endpush

@push('custom-scripts')
  <script src="{{ asset('assets/js/data-table.js') }}"></script>
  <script src="{{ asset('assets/plugins/select2/select2.min.js') }}"></script>
  <script src="{{ asset('assets/js/select2.js') }}"></script>
@endpush