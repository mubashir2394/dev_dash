@extends('layout.master')

@push('plugin-styles')
  <link href="{{ asset('assets/plugins/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet" />
@endpush

@section('content')
@auth
<div class="row">
@endauth
@guest
<div class="row" style="margin-left: -15.75rem;">
@endguest
  <div class="col-md-12">
    <div class="card">
      <div class="card-body">
        <div class="container-fluid d-flex justify-content-between">
          <div class="col-lg-3 pl-0">
            <a href="#" class="noble-ui-logo d-block mt-3"><span> BILL TO  </span></a> 
            <h6 class="text-left">{{$order->customerName}}</h6>
            <h6 class="text-left">{{$order->address}}</h6>
            <h6 class="text-left">{{$order->customerEmail}}</h6>
            <h6 class="text-left">{{$order->customerPhone}}</h6>

            <!-- <p class="mt-1 mb-1"><b>Service : gtdrg</b></p> -->
          </div>
          <div class="col-lg-3 pr-0">
            <a href="#" class="noble-ui-logo d-block text-right mt-4 mb-2"><span> INVOICE  </span></a> 
            <h6 class="text-right mb-5 pb-4"># INV-{{$order->order_id}}</h6>
            <p class="text-right mb-1">Balance Due</p>
            <h4 class="text-right font-weight-normal">$ {{$order->sub_total}}.00</h4>
            <!-- <h4 class="text-right font-weight-normal">$ 72,420.00</h4> -->
            <h6 class="mb-0 mt-3 text-right font-weight-normal mb-2"><span class="text-muted">Invoice Date :</span> {{$order->date}}</h6>
            <h6 class="text-right font-weight-normal"><span class="text-muted">Due Date :</span> {{$order->date}}</h6>
          </div>
        </div>
        <div class="container-fluid mt-5 d-flex justify-content-center w-100">
          <div class="table-responsive w-100">
              <table class="table table-bordered">
                <thead>
                  <tr>
                      <th>#</th>
                      <th>Package</th>
                      <th class="text-left">Unit Price</th>
                    </tr>
                </thead>
                <tbody>
                  @foreach($order->package_data as $key => $row)
                  <tr class="text-right">
                    <td class="text-left">{{$key+1}}</td>
                    <td class="text-left">{{$row['package']}}</td>
                    <td class="text-left">$ {{$row['price']}}.00</td>
                  </tr>
                  @endforeach  
                </tbody>
              </table>
            </div>
        </div>
        <div class="container-fluid mt-5 w-100">
          <div class="row">
            <div class="col-md-6 ml-auto">
                <div class="table-responsive">
                  <table class="table">
                      <tbody>
                        <tr>
                          <td>Sub Total</td>
                          <td class="text-right">$ {{$order->sub_total}}.00</td>
                        </tr>
                        <tr>
                        @if($order->coupon == !null)
                          <td>Applied Couopon : <b>{{$order->coupon->code." (".$order->coupon->percent."%)"}}</b></td>
                          <td class="text-right"><b>(-) $ {{$order->discount}}</b></td>
                          @else
                          <td>Applied Couopon : None</td>
                          <td class="text-right">(-) $ 0</td>
                        @endif
                        </tr>
                        <tr class="bg-light">
                          <td class="text-bold-800">Total</td>
                          <td class="text-bold-800 text-right">$ {{$order->total_amount}}</td>
                        </tr>
                      </tbody>
                  </table>
                </div>
            </div>
          </div>
        </div>
        <div class="container-fluid w-100">
         
          @auth
          <input type="hidden" value="{{ Request::fullUrl() }}" id="invoice_link">
          <button onclick="copyToClipboard('{{ Request::fullUrl() }}')" class="btn btn-primary mr-2 float-right mt-4 ml-2"><i data-feather="copy" class="mr-3 icon-md"></i>Copy Invoice Link</button>
          @endauth

          @guest
            @if($order->status == 'unpaid')
            <a href="{{ url('/'.$module.'s/stripe/'.$order->order_id) }}" class="btn btn-primary mr-2 float-right mt-4 ml-2"><i data-feather="copy" class="mr-3 icon-md"></i>Check Out</a>
            @endif
          @endguest
           
          <form method="post" action="{{ url('/'.$module.'s/cart/'.$order->order_id) }}">
          {{ csrf_field() }}
            <input type="hidden" value="1" name="pdf">
            <button type="submit" class="btn btn-outline-primary float-right mt-4"><i data-feather="printer" class="mr-2 icon-md"></i>Print</button>
        </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

<script>

  function copyToClipboard(text) {
    const elem = document.createElement('textarea');
    elem.value = text;
    document.body.appendChild(elem);
    elem.select();
    document.execCommand('copy');
    document.body.removeChild(elem);
    stylishAlert();
  }

  function stylishAlert(){
    const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })

  Toast.fire({
    icon: 'success',
    title: 'Link Copied'
  })
  }
</script>

@push('plugin-scripts')
  <script src="{{ asset('assets/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
@endpush
