@extends('layout.master')

@push('plugin-styles')
  <link href="{{ asset('assets/plugins/datatables-net/dataTables.bootstrap4.css') }}" rel="stylesheet" />
  <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" />
@endpush

@section('content')
@if (session('status'))
    <div class="alert alert-success">
        {{ ucfirst(session('status')) }}
    </div>
@endif
<div class="row">
  <div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <div class="container-fluid ml-0 pl-0">
            <div class="row">
                <div class="col-md-10">
                    <h6 class="card-title">{{$module}}s List</h6>
                </div>
                <div class="col-md-2">
                    <a href="{{ url('/'.$module.'s/add') }}" class="add-button">
                        <span class="link-title" data-toggle="tooltip" title="Add New Customer">Add New {{ucfirst($module)}}</span>
                    </a>
                </div>
            </div>
        </div>
        <!-- <h6 class="card-title">Customers Table</h6> -->
        <!-- <a href="{{ url('/customers/view') }}" class="">
                        <span class="link-title" data-toggle="tooltip" title="View">
                        <i class="link-icon" data-feather="eye"></i></span>
        </a> -->
        
        <div class="table-responsive">
          <table id="dataTableExample" class="table">
            <thead>
              <tr>
                <th>Id</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Country</th>
                <th>State</th>
                <th>Address</th>
                <th>Phone</th>
                <th>Email</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              
              @foreach($customers as $customer)
              <tr>
                <td>{{$customer->id}}</td>
                <td>{{$customer->first_name}}</td>
                <td>{{$customer->last_name}}</td>
                <td>{{$customer->country}}</td>
                <td>{{$customer->state}}</td>
                <td>{{$customer->address}}</td>
                <td>{{$customer->phone}}</td>
                <td>{{$customer->email}}</td>
                <td>
                    <!-- <a href="{{ url('/'.$module.'s/view/'.$customer->id) }}" class="">
                        <span class="link-title" data-toggle="tooltip" title="View">View |</span>
                    </a> -->
                    <a href="{{ url('/'.$module.'s/edit/'.$customer->id) }}" class="">
                        <span class="link-title" data-toggle="tooltip" title="Edit">Edit |</span>
                    </a>
                    <!-- <a href="{{ url('/'.$module.'s/delete/'.$customer->id) }}" class=""> -->
                    <a style="color:#0056b3;"  id="btnModalDel"  data-id="{{$customer->id}}">
                        <span class="link-title" data-toggle="tooltip" title="Remove"></i>Remove</span>
                    </a>
                </td>
                </tr>
                @endforeach
              
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="confirmationModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Delete Confirmation</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          Are you sure you want to delete this {{$module}} ?
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">No, Cancel</button>
          <a id="deleteCustomer" href="{{ url('/'.$module.'s/delete/'.$customer->id) }}" class="btn btn-primary">
            Yes Delete !
          </a>
        </div>
      </div>
    </div>
</div>
                   
@endsection

@push('plugin-scripts')
  <script src="{{ asset('assets/plugins/datatables-net/jquery.dataTables.js') }}"></script>
  <script src="{{ asset('assets/plugins/datatables-net-bs4/dataTables.bootstrap4.js') }}"></script>
@endpush

@push('custom-scripts')
  <script src="{{ asset('assets/js/data-table.js') }}"></script>
  <script>

  $(document).ready(function(){
      $(document).on('click','#btnModalDel',function(){

        var cusId = $(this).data('id');
        var DelURL = 'customers/delete/'+cusId;
      
        $('#deleteCustomer').attr('href',DelURL);

        $('#confirmationModal').modal('show');
      });
  });


  </script>
@endpush