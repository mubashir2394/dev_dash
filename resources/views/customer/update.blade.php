@extends('layout.master')

@push('plugin-styles')
  <link href="{{ asset('assets/plugins/datatables-net/dataTables.bootstrap4.css') }}" rel="stylesheet" />
  <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" />
@endpush

@section('content')
<div class="row">
  <div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h6 class="card-title">Update {{$module}}</h6>
        <form method="post" action="{{ url('/'.$module.'s/edit/'.$customer->id) }}">
        {{ csrf_field() }}
          <div class="form-group">
            <label>First Name</label>
            <input type="text" class="form-control" name="first" autocomplete="off" placeholder="First Name" value="{{$customer->first_name}}"/>
            @error('first')
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror
          </div>
          <div class="form-group">
            <label>Last Name</label>
            <input type="text" class="form-control" name="last" placeholder="Last Name" value="{{$customer->last_name}}"/>
            @error('last')
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror
          </div>
          <div class="form-group">
            <label>Country</label>
            <input type="text" class="form-control" name="country" autocomplete="off" placeholder="Country" value="{{$customer->country}}"/>
            @error('country')
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror
          </div>
          <div class="form-group">
            <label>State</label>
            <input type="text" class="form-control" name="state" autocomplete="off" placeholder="State" value="{{$customer->state}}"/>
            @error('state')
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror
          </div>
          <div class="form-group">
            <label>Address</label>
            <input type="text" class="form-control" name="address" autocomplete="off" placeholder="Address" value="{{$customer->address}}"/>
            @error('address')
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror
          </div>
          <div class="form-group">
            <label>Phone</label>
            <input type="text" class="form-control" name="phone" autocomplete="off" placeholder="Phone" value="{{$customer->phone}}"/>
            @error('phone')
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror
          </div>
          <div class="form-group">
            <label>Email</label>
            <input type="text" class="form-control" name="email" autocomplete="off" placeholder="Email" value="{{$customer->email}}"/>
            @error('email')
              <div class="alert alert-danger">{{ $message }}</div>
            @enderror
          </div>
          <input type="hidden" class="form-control" name="do_post" value="1"/>

          <button type="submit" class="btn btn-primary mr-2">Update</button>
          <a class="btn btn-light" href="{{ url('/'.$module.'s') }}">Cancel</a>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection

@push('plugin-scripts')
  <!-- <script src="{{ asset('assets/plugins/datatables-net/jquery.dataTables.js') }}"></script> -->
  <script src="{{ asset('assets/plugins/datatables-net-bs4/dataTables.bootstrap4.js') }}"></script>
@endpush

@push('custom-scripts')
  <script src="{{ asset('assets/js/data-table.js') }}"></script>
@endpush